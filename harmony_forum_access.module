<?php

/**
 * @file
 * harmony_forum_access.module
 */

/**
 * Helper function which will return a list of permissions when given
 * a taxonomy term.
 */
function harmony_forum_access_get_permissions($tid) {
  $term = taxonomy_term_load($tid);
  $wrapper = entity_metadata_wrapper('taxonomy_term', $term);

  // Make sure we have a machine name.
  if ($wrapper->__isset('field_harmony_machine_name')) {
    $machine_name = $wrapper->field_harmony_machine_name->value();
  }
  else {
    watchdog('harmony_forum_access', "The Harmony category term %name is missing a machine name which means access control won't work properly and default to deny.", array('%name' => $term->name));
    return array();
  }

  // Check that the term has ACL enabled.
  if (!in_array($machine_name, variable_get('harmony_forum_access_enabled_terms', array()))) {
    return array();
  }

  $permissions = array();
  $permissions["view harmony_forum category $machine_name"] = array(
    'title' => t('Access and view threads and posts in "@name"', array('@name' => $term->name)),
    'description' => t('Allow users to view this category along with threads and posts within.'),
    'op' => array('view'),
    'machine_name' => $machine_name,
  );
  $permissions["update harmony_forum category $machine_name"] = array(
    'title' => t('Update threads and replies to threads in "@name"', array('@name' => $term->name)),
    'description' => t('Allow users create threads and replies to other threads in this category.'),
    'op' => array('update'),
    'machine_name' => $machine_name,
  );
  $permissions["delete harmony_forum category $machine_name"] = array(
    'title' => t('Delete threads and replies to threads in "@name"', array('@name' => $term->name)),
    'description' => t('Allow users delete threads and replies to other threads in this category.'),
    'op' => array('delete'),
    'machine_name' => $machine_name,
  );
  $permissions["create harmony_forum category $machine_name"] = array(
    'title' => t('Create threads and replies to threads in "@name"', array('@name' => $term->name)),
    'description' => t('Allow users create threads and replies to other threads in this category. Ability to edit their own threads and posts is implied.'),
    'op' => array('create'),
    'machine_name' => $machine_name,
  );

  return $permissions;
}

/**
 * Implements hook_permission().
 */
function harmony_forum_access_permission() {
  $permissions = array(
    'administer harmony_forum_access' => array(
      'title' => t('Administer Harmony forum access'),
      'description' => t('Administer the access rules for Harmony forum categories, this permission also lets users bypass the access rules. This is so that users can alter access settings regardless of whether their roles permit access.'),
      'restrict access' => TRUE,
    ),
  );

  // Loop through taxonomy terms and create permissions for them.
  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  $vocabulary = taxonomy_vocabulary_machine_name_load($harmony_vocab);
  if ($vocabulary) {
    $terms = taxonomy_get_tree($vocabulary->vid);

    if ($terms) {
      foreach ($terms as $term) {
        $permissions = array_merge($permissions, harmony_forum_access_get_permissions($term->tid));
      }
    }
  }

  return $permissions;
}

/**
 * Implements hook_menu().
 */
function harmony_forum_access_menu() {
  $items['taxonomy/term/%harmony_forum_access_menu/forum-access'] = array(
    'title' => 'Access',
    'load arguments' => array(2),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('harmony_forum_access_form', 2),
    'access arguments' => array('administer harmony_forum_access'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 100,
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 *
 * Remove the 'Forum' menu item if no forums are visible.
 */
function harmony_forum_access_menu_alter(&$items) {
  // Term listings.
  chain_menu_access_chain($items, 'taxonomy/term/%taxonomy_term', 'harmony_forum_access_term_access', array(2));

  // Thread add.
  chain_menu_access_chain($items, 'thread/add', 'harmony_forum_access_term_access_create');
}

/**
 * Menu loader callback. Load a term and check if it's in the
 * harmony category vocabulary.
 */
function harmony_forum_access_menu_load($term_id) {
  if (!is_numeric($term_id)) {
    return FALSE;
  }

  $term = taxonomy_term_load($term_id);
  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  if (!$term || $term && $term->vocabulary_machine_name !== $harmony_vocab) {
    return FALSE;
  }

  return $term;
}

/**
 * Access callback for term page.
 */
function harmony_forum_access_term_access($term) {
  if (user_access('administer harmony_forum_access')) {
    return TRUE;
  }

  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  if ($term->vocabulary_machine_name === $harmony_vocab) {
    $wrapper = entity_metadata_wrapper('taxonomy_term', $term);

    // Make sure we have a machine name.
    $machine_name = $wrapper->__isset('field_harmony_machine_name') ? $wrapper->field_harmony_machine_name->value() : FALSE;

    if ($machine_name) {
      // Check that the term has ACL enabled.
      if (!in_array($machine_name, variable_get('harmony_forum_access_enabled_terms', array()))) {
        return TRUE;
      }
      else {
        return user_access("view harmony_forum category $machine_name");
      }
    }
    else {
      watchdog('harmony_forum_access', "The Harmony category term %name is missing a machine name which means access control won't work properly and default to deny.", array('%name' => $term->name));
      return FALSE;
    }
  }
  else {
    // Don't cause trouble.
    return TRUE;
  }
}

/**
 * Access callback which checks for attempts to prepopulate the category
 * field on the form. Catch pretenders early if we can.
 */
function harmony_forum_access_term_access_create() {
  if (user_access('administer harmony_forum_access')) {
    return TRUE;
  }

  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  $category_field = variable_get('harmony_core_category_field', 'field_harmony_category');

  if (!empty($_GET[$category_field]) && is_numeric($_GET[$category_field])) {
    // Load up the term to check it exists and that it's in the correct vocab.
    $term = taxonomy_term_load($_GET[$category_field]);

    // @ todo check access.
    if ($term && $term->vocabulary_machine_name === $harmony_vocab) {
      $wrapper = entity_metadata_wrapper('taxonomy_term', $term);

      // Make sure we have a machine name.
      $machine_name = $wrapper->__isset('field_harmony_machine_name') ? $wrapper->field_harmony_machine_name->value() : FALSE;

      if ($machine_name) {
        // Check that the term has ACL enabled.
        if (!in_array($machine_name, variable_get('harmony_forum_access_enabled_terms', array()))) {
          return TRUE;
        }
        else {
          return user_access("create harmony_forum category $machine_name");
        }
      }
      // On the offchance that we don't have a machine name for some reason,
      // assume the worst. Also watchdog.
      else {
        watchdog('harmony_forum_access', "The Harmony category term %name is missing a machine name which means access control won't work properly and default to deny.", array('%name' => $term->name));
        return FALSE;
      }
    }
  }
  // Chain menu access needs us to return TRUE if we're ignoring.
  else {
    return TRUE;
  }
}

/**
 * Implements hook_admin_paths().
 */
function harmony_forum_access_admin_paths() {
  $paths = array(
    'taxonomy/term/*/forum-access' => TRUE,
    'taxonomy/term/*/forum-access/*' => TRUE,
  );

  return $paths;
}

function harmony_forum_access_form($form, $form_state, $term) {
  // Grab the term machine name.
  $wrapper = entity_metadata_wrapper('taxonomy_term', $term);
  $machine_name = $wrapper->__isset('field_harmony_machine_name') ? $wrapper->field_harmony_machine_name->value() : NULL;

  if (!$machine_name) {
    watchdog('harmony_forum_access', "The Harmony category term %name is missing a machine name which means access control won't work properly and default to deny.", array('%name' => $term->name));
    return array(
      'warning' => array(
        '#markup' => t("<p>This term is missing a machine name which means access control won't work properly, please refer to the module readme.</p>"),
      ),
    );
  }

  $form['guidance_text'] = array(
    '#markup' => t("<p>Once access control is enabled, the permissions will apply to the category and threads within this category.</p>"),
  );

  $form['machine_name'] = array(
    '#type' => 'value',
    '#value' => $machine_name,
  );

  // Work out if ACL is enabled for this term.
  $term_enabled = (int)in_array($machine_name, variable_get('harmony_forum_access_enabled_terms', array()));
  $form['acl_enabled'] = array(
    '#title' => t('Enable access control for this forum category'),
    '#type' => 'checkbox',
    '#default_value' => $term_enabled,
    '#description' => t('By enabling this permissions will be created for this forum category, following submission these permissions will be available for configuration.'),
  );

  $form['acl_enabled_original'] = array(
    '#type' => 'value',
    '#value' => $term_enabled,
  );

  if ($term_enabled) {
    // Add the field permissions matrix itself. Wait until the #pre_render stage
    // to move it to the above container, to avoid having the permissions data
    // saved as part of the field record.
    $form['term_permissions']['#tree'] = TRUE;
    $form['term_permissions']['permissions'] = harmony_forum_access_permissions_matrix($term);
    $form['term_permissions']['permissions']['#states'] = array(
      'visible' => array(
        ':input[id="edit-acl-enabled"]' => array('checked' => TRUE),
      ),
    );
    //$form['#pre_render'][] = '_harmony_forum_access_form_pre_render';
  }

  // Add a submit handler to process the field permissions settings. Note that
  // it is important for this to run *after* the main field UI submit handler
  // (which saves the field itself), since when a new field is being created,
  // our submit handler will try to assign any new custom permissions
  // immediately, and our hook_permission() implementation relies on the field
  // info being up-to-date in order for that to work correctly.
  $form['#submit'][] = 'harmony_forum_access_form_submit';

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#weight' => 5,
  );

  return $form;
}

function harmony_forum_access_permissions_matrix($term) {
  // This function primarily contains a simplified version of the code from
  // user_admin_permissions().
  $form['#theme'] = 'user_admin_permissions';
  $options = array();
  $status = array();

  // Retrieve all role names for use in the submit handler.
  $role_names = user_roles();
  $form['role_names'] = array(
    '#type' => 'value',
    '#value' => $role_names,
  );

  // Retrieve the permissions for each role, and the field permissions we will
  // be assigning here.
  $role_permissions = user_role_permissions($role_names);
  $term_permissions = harmony_forum_access_get_permissions($term->tid);

  // Go through each field permission we will display.
  foreach ($term_permissions as $permission => $info) {
    // Display the name of the permission as a form item.
    $form['permission'][$permission] = array(
      '#type' => 'item',
      '#markup' => $info['title'],
    );
    // Save it to be displayed as one of the role checkboxes.
    $options[$permission] = '';

    foreach ($role_names as $rid => $name) {
      if (isset($role_permissions[$rid][$permission])) {
        $status[$rid][] = $permission;
      }
    }
  }

  // Build the checkboxes for each role.
  foreach ($role_names as $rid => $name) {
    $form['checkboxes'][$rid] = array(
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => isset($status[$rid]) ? $status[$rid] : array(),
      '#attributes' => array('class' => array('rid-' . $rid)),
    );
    $form['role_names'][$rid] = array('#markup' => check_plain($name), '#tree' => TRUE);
  }

  // Attach the default permissions page JavaScript.
  $form['#attached']['js'][] = drupal_get_path('module', 'user') . '/user.permissions.js';

  // Attach our custom JavaScript for the permission matrix.
  $form['#attached']['js'][] = drupal_get_path('module', 'field_permissions') . '/field_permissions.admin.js';

  return $form;
}

function harmony_forum_access_form_submit($form, &$form_state) {
  // ACL has been turned on where it previously wasn't.
  if ($form_state['values']['acl_enabled'] && !$form_state['values']['acl_enabled_original']) {
    $machine_name = $form_state['values']['machine_name'];

    $enabled_terms = variable_get('harmony_forum_access_enabled_terms', array());
    $enabled_terms[] = $machine_name;

    variable_set('harmony_forum_access_enabled_terms', array_unique($enabled_terms));
  }
  // Turn it off.
  elseif (!$form_state['values']['acl_enabled'] && $form_state['values']['acl_enabled_original']) {
    $enabled_terms = variable_get('harmony_forum_access_enabled_terms', array());
    $machine_name = $form_state['values']['machine_name'];

    if (($key = array_search($machine_name, $enabled_terms)) !== FALSE) {
      unset($enabled_terms[$key]);
      harmony_access_rebuild(TRUE);
    }

    variable_set('harmony_forum_access_enabled_terms', $enabled_terms);
  }

  // Update the permissions, access control is on and stays on.
  if ($form_state['values']['acl_enabled'] && $form_state['values']['acl_enabled_original']) {
    $term_permissions = $form_state['values']['term_permissions']['permissions'];
    foreach ($term_permissions['role_names'] as $rid => $name) {
      user_role_change_permissions($rid, $term_permissions['checkboxes'][$rid]);
    }

    // Clear caches.
    drupal_set_message(t('Permissions updated!'));
    harmony_access_rebuild(TRUE);
  }
  else {
    drupal_set_message(t('Settings saved.'));
  }

  cache_clear_all();
}

/**
 * Implements hook_taxonomy_term_delete().
 *
 * React to a term being deleted, if it was ACL was enabled on it remove
 * it from the array and flag access records for rebuild.
 */
function harmony_forum_access_taxonomy_term_delete($term) {
  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  if ($term->vocabulary_machine_name === $harmony_vocab) {
    $wrapper = entity_metadata_wrapper('taxonomy_term', $term);

    // Make sure we have a machine name.
    if ($wrapper->__isset('field_harmony_machine_name')) {
      // Remove from enabled terms if there.
      $machine_name = $wrapper->field_harmony_machine_name->value();
      $enabled_terms = variable_get('harmony_forum_access_enabled_terms', array());

      if (($key = array_search($machine_name, $enabled_terms)) !== FALSE) {
        unset($enabled_terms[$key]);
        harmony_access_rebuild(TRUE);
      }

      variable_set('harmony_forum_access_enabled_terms', $enabled_terms);
    }
  }
}

/**
 * Implements hook_harmony_access_grants().
 */
function harmony_forum_access_harmony_access_grants($account, $op, $machine_names = TRUE) {
  static $user_access_grants = array();
  $static_index = $machine_names ? 'mn' : 'tids';

  if (!$account) {
    global $user;
    $account = $user;
  }

  if (isset($user_access_grants[$account->uid][$static_index])) {
    return $user_access_grants[$account->uid][$static_index];
  }

  $enabled_terms = variable_get('harmony_forum_access_enabled_terms', array());
  $account_roles = isset($account->roles) && is_array($account->roles) ? $account->roles : array();
  $role_permissions = user_role_permissions($account_roles);
  $permissions = array();
  $grants = array();

  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  $vocabulary = taxonomy_vocabulary_machine_name_load($harmony_vocab);
  if ($vocabulary) {
    $terms = taxonomy_get_tree($vocabulary->vid);

    if ($terms) {
      foreach ($terms as $term) {
        $term = taxonomy_term_load($term->tid);
        $wrapper = entity_metadata_wrapper('taxonomy_term', $term);

        // Make sure we have a machine name.
        if ($wrapper->__isset('field_harmony_machine_name')) {
          $machine_name = $wrapper->field_harmony_machine_name->value();

          foreach ($account_roles as $rid => $name) {
            if (isset($role_permissions[$rid]["$op harmony_forum category $machine_name"]) && $role_permissions[$rid]["$op harmony_forum category $machine_name"]) {
              $grants['mn']["hfa:$machine_name"][] = $rid;
              $grants['tids'][$term->tid][] = $rid;
            }
          }
        }

        unset($wrapper);
      }
    }
  }

  $user_access_grants[$account->uid] = $grants;
  return !empty($user_access_grants[$account->uid][$static_index]) ? $user_access_grants[$account->uid][$static_index] : array();
}

/**
 * Implements hook_harmony_access_records().
 */
function harmony_forum_access_harmony_access_records($entity, $entity_type) {
  if ($entity_type !== 'harmony_thread') {
    return array();
  }

  // Only act if the thread has a category, and if that category has a machine name.
  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  $category_field = variable_get('harmony_core_category_field', 'field_harmony_category');
  $wrapper = entity_metadata_wrapper($entity_type, $entity);
  if (!$wrapper->__isset($category_field) || $wrapper->__isset($category_field) && !$wrapper->{$category_field}->value() || $wrapper->__isset($category_field) && $wrapper->{$category_field}->value() && !$wrapper->{$category_field}->__isset('field_harmony_machine_name')) {
    return array();
  }

  // Grab and work out the machine name and permissions.
  $enabled_terms = variable_get('harmony_forum_access_enabled_terms', array());
  $machine_name = $wrapper->{$category_field}->field_harmony_machine_name->value();

  if (!in_array($machine_name, $enabled_terms)) {
    return array();
  }

  $permissions = array();
  $vocabulary = taxonomy_vocabulary_machine_name_load($harmony_vocab);
  if ($vocabulary) {
    $terms = taxonomy_get_tree($vocabulary->vid);

    if ($terms) {
      foreach ($terms as $term) {
        $term = taxonomy_term_load($term->tid);
        $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term);

        if ($term_wrapper->field_harmony_machine_name->value() === $machine_name) {
          $permissions = harmony_forum_access_get_permissions($term->tid);
          break;
        }

        unset($terms_wrapper);
      }
    }
  }

  $role_names = user_roles();
  $role_permissions = user_role_permissions($role_names);
  $grants = array();

  foreach ($role_permissions as $rid => $permission_set) {
    $grant_view = 0;
    $grant_update = 0;
    $grant_delete = 0;
    $grant_create = 0;

    // View.
    if (isset($permission_set["view harmony_forum category $machine_name"]) && $permission_set["view harmony_forum category $machine_name"] == 1) {
      /**
       * Published entities.
       */
      if ($entity->status) {
        $grant_view = 1;
      }
      /**
       * Unpublished.
       */
      else {
        if (isset($permission_set["view unpublished {$entity_type}s"]) && $permission_set["view unpublished {$entity_type}s"] == 1) {
          $grant_view = 1;
        }
      }
    }

    // Update.
    if (isset($permission_set["update harmony_forum category $machine_name"]) && $permission_set["update harmony_forum category $machine_name"] == 1) {
      $grant_update = 1;
    }

    // Delete.
    if (isset($permission_set["delete harmony_forum category $machine_name"]) && $permission_set["delete harmony_forum category $machine_name"] == 1) {
      $grant_delete = 1;
    }

    // Create.
    if (isset($permission_set["create harmony_forum category $machine_name"]) && $permission_set["create harmony_forum category $machine_name"] == 1) {
      $grant_create = 1;
    }

    if ($grant_view + $grant_update + $grant_delete + $grant_create > 0) {
      $grants[] = array(
        'realm' => 'hfa:' . $machine_name,
        'gid' => $rid,
        'grant_view' => $grant_view,
        'grant_update' => $grant_update,
        'grant_delete' => $grant_delete,
        'grant_create' => $grant_create,
        'priority' => 0,
      );
    }
  }

  return $grants;
}

/**
 * Get a list of the harmony_category term id's that a user has access to.
 */
function harmony_forum_access_get_user_tids($account = NULL, $op = 'view') {
  static $user_tids = array();

  if (!$account) {
    global $user;
    $account = $user;
  }

  if (isset($user_tids[$account->uid][$op])) {
    return $user_tids[$account->uid][$op];
  }

  // Load up all terms then exclude ACL ones.
  $enabled_terms = variable_get('harmony_forum_access_enabled_terms', array());
  $tids = array();
  $harmony_vocab = variable_get('harmony_core_category_vocabulary', 'harmony_category');
  $vocabulary = taxonomy_vocabulary_machine_name_load($harmony_vocab);
  if ($vocabulary) {
    $terms = taxonomy_get_tree($vocabulary->vid, 0, NULL, TRUE);

    if ($terms) {
      foreach ($terms as $term) {
        $wrapper = entity_metadata_wrapper('taxonomy_term', $term);

        // Make sure we have a machine name.
        if ($wrapper->__isset('field_harmony_machine_name')) {
          $machine_name = $wrapper->field_harmony_machine_name->value();

          if (!in_array($machine_name, $enabled_terms)) {
            $tids[] = $term->tid;
          }
        }
      }
    }
  }

  $grants = harmony_forum_access_harmony_access_grants($account, $op, FALSE);

  if (!empty($grants)) {
    foreach ($grants as $tid => $grant) {
      $tids[] = $tid;
    }
  }

  $user_tids[$account->uid][$op] = $tids;
  return $user_tids[$account->uid][$op];
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * On the thread add/edit form check that the term reference field
 * options contain terms which the user has access to create within.
 */
function harmony_forum_access_form_harmony_core_thread_form_alter(&$form, &$form_state, $form_id) {
  // @todo Bundle support.
  $category_field = variable_get('harmony_core_category_field', 'field_harmony_category');
  if (!empty($form[$category_field]) && !user_access('bypass harmony forum access control')) {
    harmony_forum_access_alter_options($form[$category_field][$form[$category_field]['#language']]['#options']);
    $options = $form[$category_field][$form[$category_field]['#language']]['#options'];

    if (empty($options) || !empty($options) && count($options) === 1 && isset($options['_none'])) {
      $form[$category_field]['#access'] = FALSE;
      $form[$category_field]['#access_note'] = 'Hidden by harmony_forum_access module.';
    }
  }
}

/**
 * Helper function to remove options the user doesn't have access to.
 */
function harmony_forum_access_alter_options(&$options) {
  global $user;
  $new_options = array();
  $can_access = harmony_forum_access_get_user_tids($user, 'create');

  foreach ($options as $tid => $name) {
    if (in_array($tid, $can_access)) {
      $new_options[$tid] = $name;
    }
  }

  if (!empty($new_options) && isset($options['_none'])) {
    $new_options = array($options['_none']) + $new_options;
  }

  $options = $new_options;
}

/**
 * Implements hook_harmony_category_options_alter().
 */
function harmony_forum_access_harmony_category_options_alter(&$options) {
  if (!user_access('bypass harmony forum access control')) {
    harmony_forum_access_alter_options($options);
  }
}

/**
 * Implements hook_query_TAG_alter().
 *
 * Jump in when grabbing a term from harmony_category to check that the
 * user has access via the view permission.
 */
function harmony_forum_access_query_harmony_forum_access_alter(QueryAlterableInterface $query) {
  global $user;

  if (!$account = $query->getMetaData('account')) {
    $account = $user;
  }

  // Does the user have permission to side step Harmony access checks.
  if (user_access('bypass harmony forum access control', $account)) {
    return;
  }
  // Check for bypass tag.
  if ($query->hasTag('harmony_access_bypass')) {
    return;
  }

  $alias = '';
  $tables =& $query->getTables();
  foreach ($tables as $i => $table) {
    if ($table['table'] === 'taxonomy_term_data') {
      $alias = $table['alias'];
      break;
    }
  }
  if (empty($alias)) {
    return;
  }

  // Get the term id's that the user has access to.
  $tids = harmony_forum_access_get_user_tids($account);

  if (!empty($tids)) {
    $query->condition($alias . ".tid", $tids, "IN");
  }
  else {
    return;
  }
}
