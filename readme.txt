How do I use this??

Navigate to a taxonomy term in the Harmony Category vocabulary, click on the "Access" tab, enable access control for the term and then you'll get some permissions! Set as necessary.

Help:
What do the messages about terms missing a machine name about? Well, so that we can feature our ACL configuration we need machine names to rely on instead of term ids, or hefty UUIDs so we use machine name. You'll want to check the terms in Harmony Category which are missing a machine name and add one, after that it should be fine.
